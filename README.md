# m2101

Projet pour le module m2101 portant sur le chiffrement de messages.

Usage des fonctions :

- getMessage sert à récupérer le message et à le stocker
- getValeurAscii sert à obtenir la valeur ASCII d'un caractère
- remplaceAccents sert à remplacer les caractères accentués dans un texte
- testCaracteres sert à vérifier certaines conditions sur un caractère
- chiffrementCesar sert à appliquer le code César pour crypter un message donné
- dechiffreCesar permet de transformer un message crypté par le chiffre de César en texte clair

**Algorithmes :**

Algorithme de César :
- L'algorithme de César se base sur un codage simple : on décale la position de chaque lettre pour créer le code.
- La clé correspond à un entier qui donne le décalage à effectuer sur la droite. Par exemple, avec une clé de 3, la lettre A devient D.

Algorithme de Vigenère :
- Cet algorithme se base sur un principe similaire à celui du chiffre de César, mais contrairement à celui-ci qui est mono-alphabétique, ici la méthode se base sur plusieurs caractères.
- La clé est ici une phrase ou un mot, que l'on peut répéter si besoin, qui permet de décaler caractère par caractère la position pour le chiffrement.
- Pour la table de Vigenère : https://fr.wikipedia.org/wiki/Chiffre_de_Vigen%C3%A8re#La_table_de_Vigen%C3%A8re
