/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Mohamedi-Estebe Yanis                                        *
*                                                                             *
*  Nom-prénom2 : Durand Paul-Adrien                                           *
*                                                                             *
*******************************************************************************
*  Nom du fichier : main.c                                                    *
*                                                                             *
******************************************************************************/

//imports
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "clearBuffer.h"
#include "dechiffreCesar.h"
#include "chiffrementCesar.h"
#include "getMessage.h"
#include "remplaceAccents.h"
#include "testCaracteres.h"

void main () {
    //flag sert aux diverses conditions du main
    int flag = 0;
    //variable servant a savoir si on crypte ou decrypte
    int cryptageOuDecryptage;
    //cle de cryptage
    int cle;
    //message
    wchar_t * message;
    printf("Bonjour bienvenue dans ce codeur decodeur de message\n");
    printf("Ici on code et decode des messages avec l'algorithmes de cesar\n");
    printf("Veuillez rentrer votre message et ne pas mettre de caracteres speciaux\n");
    //on lit le message de l'utilisateur
    message = getMessage();
    //on nettoie le buffer
    clearBuffer();
    //on regarde si il y a des caracteres speciaux
    for (int i =0; i<wcslen(message);i++){
        if (estCaractereSpecial(message[i])) {
            //si oui on augmente flag
            flag ++;
        }
    }
    //tant que le message contient des caracteres speciaux 
    while (flag > 0) {
        //on remet flag a 0
        flag = 0;
        printf("Veuillez ne pas mettre de caracteres speciaux dans le message\n");
        printf("Veuillez de nouveau rentrer votre message\n");
        //on refait rentrer le message
        message = getMessage();
        //on nettoie le buffer
        clearBuffer();
        //on regarde si il y a des caracteres speciaux
        for (int i =0; i<wcslen(message);i++){
            if (estCaractereSpecial(message[i])) {
                //on incremente flag si le caractere teste est un caractere special
                flag ++;
            }
        }
    }
    //on remet flag a 0
    flag = 0;
    for (int i =0; i<wcslen(message);i++){
        //si le caractere teste n'est ni alphanumerique ni special alors c'est un caractere accentue
        if (!estCaractereSpecial(message[i]) && !estAlphanumerique(message[i])) {
            flag++;
        }
    }
    //on remplace les caracteres accentues
    if (flag > 0) {
        remplaceAccents(message);
    }
    //on remet flag a 0
    flag = 0;
    //on nettoie le buffer
    clearBuffer();
    printf("Veuillez rentrer la cle pour chiffrer dechiffrer le message\n");
    //on affecte une valeur a la cle
    scanf("%d\n",&cle);
    //on nettoie le buffer
    clearBuffer();
    printf("Souhaitez vous crypter le message ou le decrypter ?\n");
    printf("1 pour le cryptage\n");
    printf("2 pour le decryptage\n");
    //on demande a l'utilisateur si il crypte ou decrypte
    scanf("%d\n",&cryptageOuDecryptage);
    //on nettoie le buffer
    clearBuffer();
    //si l'utilisateur choisit de crypter ou decrypter alors on effectue l'operation correspondante
    switch (cryptageOuDecryptage)
    {
    case 1:
        chiffrementCesar(message,cle);
        break;

    case 2:
        dechiffreCesar(message,cle);
        break;
    
    default:
        //sinon on incremente flag
        flag++;
    }
    //tant que l'utilisateur ne choisit pas une option valide
    while(flag > 0) {
        //on remet flag a zero
        flag = 0;
        printf("Veuillez rentrer un chiffre correct\n");
        printf("1 pour le cryptage\n");
        printf("2 pour le decryptage\n");
        //on redemande a l'utilisateur si il crypte ou decrypte
        scanf("%d\n",&cryptageOuDecryptage);
        clearBuffer();
        //si l'utilisateur choisit de crypter ou decrypter alors on effectue l'operation correspondante
        switch (cryptageOuDecryptage)
        {
        case 1:
            chiffrementCesar(message,cle);
            break;

        case 2:
            dechiffreCesar(message,cle);
            break;
        
        default:
            //sinon on incremente flag
            flag++;
        }
    }
    //on affiche ensuite le message
    printf("Votre message est donc :\n");
    wprintf(L"%ls\n",message);
}