/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Mohamedi-Estebe Yanis                                        *
*                                                                             *
*  Nom-prénom2 : Durand Paul-Adrien                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : testCaracteres.c                                          *
*                                                                             *
******************************************************************************/

//imports
#include <stdbool.h>
#include <stdlib.h>
#include "testCaracteres.h"
#include <wchar.h>

//vrai si le caratere lettre est alphanumerique
bool estAlphanumerique(wchar_t lettre) {
    const wchar_t caracteresAlphanumeriques[64] = {'a','b','c','d','e','f','g','h','i','j','k',
                                            'l','m','o','n','p','q','r','s','t','u','v','w',
                                            'x','y','z','A','B','C','D','E','F','G','H','I',
                                            'J','K','L','M','N','O','P','Q','R','S','T','U',
                                            'V','W','X','Y','Z','1','2','3','4','5','6','7',
                                            '8','9','0'};
    for (int i = 0; i<64;i++) {
        if (caracteresAlphanumeriques[i] == lettre) {
            return true;
        }
    }
    return false;
}

//renvoie vrai si le caractere lettre est un caractere special (caracteres accentues non inclus)
bool estCaractereSpecial(wchar_t lettre) {
    const wchar_t caracteresSpeciaux[40] = {'>','<','#','(','{','[',']','}',')','/','*','-','+','.',
                                            '@','|','&','~','"','_','`','^','?',';',',',':','!','%',
                                            '$',L'§',L'µ',L'£',L'¤','*',L'€','=',L'°',L'²','\'','\\'};
    for (int i = 0; i<40;i++) {
        if (caracteresSpeciaux[i] == lettre) {
            return true;
        }
    }
    return false;
}
