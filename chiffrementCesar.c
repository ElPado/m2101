/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N de sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitule : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prenom1 : Mohamedi-Estebe Yanis                                        *
*                                                                             *
*  Nom-prenom2 : Durand Paul-Adrien                                           *
*                                                                             *
*******************************************************************************                                                                            *
*  Nom du fichier : chiffrementCesar.c                                        *
*                                                                             *
******************************************************************************/

//imports
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include "chiffrementCesar.h"

void chiffrementCesar(wchar_t *message, int clef){

    //boucle sur la longueur du message
    for(unsigned i = 0; i < wcslen(message); i++){

        //cas o� le caract�re est une lettre minuscule
        if(message[i] >= 'a' && message[i] <= 'z'){
            message[i] = 'a' + ((message[i] - 'a') + clef) % 26;
        }
        //cas o� le caract�re est une lettre majuscule
        if(message[i] >= 'A' && message[i] <= 'Z'){
            message[i] = 'A' + ((message[i] - 'A') + clef) % 26;
        }
        //cas o� le caract�re est un chiffre
        if(message[i] >= '0' && message[i] <= '9'){
            message[i] = '0' + ((message[i] - '0') + clef) % 10;
        }
    }
}
