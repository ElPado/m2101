GCC = gcc
SOURCES = $(wildcard *.c)
HEADERS = $(SOURCES:.c=.h)
BINAIRES = $(SOURCES:.c=.o)

all : main

main : ${BINAIRES}

%.o : %.c %.h
	${GCC} -c ${SOURCES}

clean :
	rm -f *.o
	rm -f ./main

archive : 
	zip -r sujet3_DURAND_Paul-Adrien_MOHAMEDI-ESTEBE_Yanis.zip ${SOURCES} ${HEADERS} makefile README.md