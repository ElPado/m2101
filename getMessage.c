/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Mohamedi-Estebe Yanis                                        *
*                                                                             *
*  Nom-prénom2 : Durand Paul-Adrien                                           *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier : getMessage.c                                              *
*                                                                             *
******************************************************************************/

//imports
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>
#include "getMessage.h"

//ce programme permet de lire dynamiquement une chaine de caracteres rentres par l'utilisateur
wchar_t * getMessage() {

    //la chaine
    wchar_t * message = malloc(sizeof(wchar_t));
    //caractere que l'on ajoutera a la chaine
    char c;
    //indice utilise dans la boucle
    int i=0;

    //tant que l'utilisateur ne fait pas de retour a la ligne
    while(c != '\n'){
        //on affecte a c la valeur du prochain entier rentre par l'utilisateur
        c=getchar();
        //on augmente la taille de la chaine
        message = realloc(message,(i+1)*sizeof(wchar_t));
        //on rajoute c a la chaine
        message[i] = c;
        //incrementation de l'indice
        i++;
    }
    //on augmente la taille de la chaine
    message = realloc(message,(i+1)*sizeof(wchar_t));
    //ajout du caractere de fin de chaine
    message[i] ='\0';

    return message;
}
