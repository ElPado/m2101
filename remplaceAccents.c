/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé : Chiffrement de messages                                         *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Mohamedi-Estebe Yanis                                        *
*                                                                             *
*  Nom-prénom2 : Durand Paul-Adrien                                           *
*                                                                             *
*******************************************************************************                                                                            *
*
Nom du fichier : remplaceAccents.c                                            *
*                                                                             *
******************************************************************************/

//imports
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>

void remplaceAccents(wchar_t *message){
    wchar_t listeAccentue[54] = L"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÌÍÎÏìíîïÙÚÛÜùúûüÿÑñÇç";
    wchar_t listeNonAccentue[54] = L"AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc";

    for(unsigned i = 0; i < wcslen(message); i++){
        for(unsigned j = 0; j < 54; j++){
            if(message[i] == listeAccentue[j]){
                message[i] = listeNonAccentue[j];
            }
        }
    }
}
